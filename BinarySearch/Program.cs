﻿int[] ints = { 1, 2, 3, 4, 5, 7, 10, 15, 26, 66, 76, 84, 86 };
int BinarySearch(int[] ints, int value)
{
    int left = 0;
    int right = ints.Length;
    int mid;
    for (int i = left; i < right; i++)
    {
        mid = (left + right) / 2;
        if (ints[mid] == value)
        {
            return ints[mid];
        }
        if (ints[mid] > value)
        {
            right = mid - 1;
        }
        if (ints[mid] < value)
        {
            left = mid + 1;
        }
    }
    throw new Exception("Can not found");
}

int value = BinarySearch(ints, 86);
Console.WriteLine(value);