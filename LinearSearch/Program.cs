﻿int[] ints = { 1, 2, 3, 4, 5, 6, 9 };

int LinearSearch(int[] ints, int value)
{
    foreach (int i in ints)
    {
        if(i == value)
        {
            return i;
        }
    }
    throw new Exception("Can not found !");
}

int value = LinearSearch(ints, 7);
Console.WriteLine(value);