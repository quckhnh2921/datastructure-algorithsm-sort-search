﻿int[] ints = { 5, 4, 7, 3, 8, 2, 4, 9, 10 };

void SelectionSort(int[] ints)
{
    for (int i = 0; i < ints.Length; i++)
    {
        int min = i;
        for (int j = i; j < ints.Length; j++)
        {
            if (ints[j] < ints[min])
            {
                int temp = ints[min];
                ints[min] = ints[j]; 
                ints[j] = temp;
            }
        }
    }
}

void Print(int[] ints)
{
    foreach (int i in ints)
    {
        Console.Write(i+" ");
    }
}

SelectionSort(ints);
Print(ints);