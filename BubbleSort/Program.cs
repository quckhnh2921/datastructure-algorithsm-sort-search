﻿int[] ints = { 10, 2, 6, 5, 7, 8, 3, 8, 1 };

void BubbleSort(int[] arr)
{
    for (int i = 0; i < arr.Length - 1; i++)
    {
        for (int j = 0; j < arr.Length - 1; j++)
        {
            if (arr[j] > arr[j + 1])
            {
                var temp = arr[j + 1];
                arr[j + 1] = arr[j];
                arr[j] = temp;
            }
        }
    }
}

void Print(int[] arr)
{
    foreach (int i in arr)
    {
        Console.Write(i + " ");
    }
}

BubbleSort(ints);
Print(ints);